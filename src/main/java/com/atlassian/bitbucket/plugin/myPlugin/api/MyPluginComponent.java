package com.atlassian.bitbucket.plugin.myPlugin.api;

public interface MyPluginComponent
{
    String getName();
}